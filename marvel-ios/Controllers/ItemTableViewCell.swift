//
//  StoriesTableViewCell.swift
//  marvel-ios
//
//  Created by Admin on 11.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var descriptionItem: UILabel!
    @IBOutlet weak var imageItem: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: - Public
    var item: ItemEntity? {
        didSet {
            self.selectionStyle = UITableViewCellSelectionStyle.none
            itemName.text = item?.title
          
            guard let imagePath = item?.imagePath else {return}
            guard let imageExtension = item?.imageExtension else {return}
            guard let imageURL = URL.init(string: String(format: "%@.%@", imagePath, imageExtension)) else { return }
            imageItem.kf.setImage(with: imageURL)
            imageItem.contentMode = .scaleAspectFill
            imageItem.layer.borderColor = UIColor.lightGray.cgColor
            imageItem.layer.borderWidth = 1.0
            imageItem.layer.masksToBounds = false
//            imageItem.layer.cornerRadius = imageItem.frame.size.width/2
            imageItem.clipsToBounds = true
            
            descriptionItem.text = item?.itDescription
        }
    }
}
