//
//  HerosCollectionViewController.swift
//  marvel-ios
//
//  Created by Admin on 09.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit
import CoreData
import MagicalRecord
import ObjectMapper

private let reuseIdentifier = "HeroCollectionViewCellID"
private let loadCapacity = 20

extension UIAlertController{
    //MARK: - UIAelrtController
    static func createErorAlert(error: Error) -> UIAlertController{
        print(error.localizedDescription)
        let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
        return alertController
    }
}

class HerosCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout{
    
    // MARK: - IBOutlets
    @IBOutlet weak internal var collectionOfHeros: UICollectionView!
    
    // MARK: - Variables
    let imageHeight: CGFloat = 170
    let offsetSpeed: CGFloat = 10.0
    var refreshControl: UIRefreshControl!
    var loadMoreStatus = false
    var heroesCollectionArray: [HeroEntity] = []
    var heroesDataObject = [NSManagedObject]()
    let requestManager = RequestManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register cell classes
        let heroCellNib = UINib(nibName: "HeroCollectionViewCell", bundle: nil)
        self.collectionView?.register(heroCellNib, forCellWithReuseIdentifier: reuseIdentifier)
    
        //refreshControl
        refreshControl = UIRefreshControl.init()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refreshCollectionView(sender:)), for: UIControlEvents.valueChanged)
        collectionOfHeros.addSubview(refreshControl) // not required when using UITableViewController
        
        //load and draw heroes
        requestManager.getHeroes(offset: heroesCollectionArray.count, { [weak self] (heroes, error) in
            guard let sself = self else { return }
            guard let heroes = heroes else{
                print(error?.localizedDescription ?? "Error")
                let sortSavedHeroes = HeroEntity.pullHeroEntity(startIndex: 0, capacity: loadCapacity)
                guard sortSavedHeroes.count > 0 else {
                    sself.present(UIAlertController.createErorAlert(error: error!), animated: true, completion: nil)
                    return
                }
                sself.heroesCollectionArray = sortSavedHeroes
                sself.collectionOfHeros.reloadData()
                return
            }
            sself.heroesCollectionArray = heroes
            NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
            sself.collectionOfHeros.reloadData()
        })
        // Do any additional setup after loading the view.
    }
    
    // MARK: - UICollectionViewDataSource
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return heroesCollectionArray.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let hero = heroesCollectionArray[indexPath.item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! HeroCollectionViewCell
        cell.hero = hero
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds.size
        return CGSize(width: screenSize.width / 2 - 40, height: screenSize.width / 2 - 40 + 23)
    }

    // MARK: - UICollectionViewDelegate
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        print("id = \(heroesCollectionArray[indexPath.item].id) name = \(heroesCollectionArray[indexPath.item].name)")
        self.performSegue(withIdentifier: "showFullInformation", sender: heroesCollectionArray[indexPath.item])
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showFullInformation"{
            let hero = sender as! HeroEntity
            if let destinationController = segue.destination as? HeroFullInformationController{
                destinationController.hero = hero
            }
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //for images parallax
        if let visibleCells = collectionOfHeros.visibleCells as? [HeroCollectionViewCell] {
            for heroCell in visibleCells {
                var yOffset = (((collectionView?.contentOffset.y)! - heroCell.frame.origin.y) / imageHeight) * offsetSpeed
                heroCell.offset(offset: CGPoint.init(x: 0.0, y: yOffset))
            }
        }
        
        //display Heroes
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height < collectionOfHeros.frame.size.height ? collectionOfHeros.frame.size.height : scrollView.contentSize.height
        let maxiHeight = contentHeight - collectionOfHeros.frame.size.height
        loadMoreStatus = offsetY < 2/3 * maxiHeight ? false : loadMoreStatus
        print(" 1>>> \(offsetY) > \(contentHeight - collectionOfHeros.frame.size.height)")

        if offsetY > 2/3 * maxiHeight && !loadMoreStatus {
            loadMoreStatus = true
            var indexPaths = [IndexPath]()
            let offset = self.collectionOfHeros.numberOfItems(inSection: 0) ?? 0
            print(" >>>> offset: \(offset)")
            self.requestManager.getHeroes (offset: offset, { [weak self] (heroes, error) in
                guard let sself = self else { return }
                guard let heroes = heroes else {
                    print(error?.localizedDescription ?? "Error")
                    let sortSavedHeroes = HeroEntity.pullHeroEntity(startIndex: offset, capacity: loadCapacity)
                    guard sortSavedHeroes.count > 0 else {
                        sself.present(UIAlertController.createErorAlert(error: error!), animated: true, completion: nil)
                        return
                    }
                    sortSavedHeroes.forEach({ (hero) in
                        sself.heroesCollectionArray.append(hero)
                        indexPaths.append(IndexPath.init(item: (sself.heroesCollectionArray.endIndex)-1, section: 0))
                    })
                    sself.collectionOfHeros.insertItems(at: indexPaths)
                    return
                }
                heroes.forEach({ (hero) in
                    sself.heroesCollectionArray.append(hero)
                    indexPaths.append(IndexPath.init(item: (sself.heroesCollectionArray.endIndex)-1, section: 0))
                })
                sself.collectionOfHeros.insertItems(at: indexPaths)
                print(" 2>>> \(scrollView.contentOffset.y) > \(contentHeight - sself.collectionOfHeros.frame.size.height)")
                NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
            })
        }
    }

    //MARK: - UIRefreshControl, pull to refresh
    func refreshCollectionView(sender:AnyObject) {
        self.requestManager.getHeroes(offset: 0, { [weak self] (heroes, error) in
            self?.refreshControl.endRefreshing()
            guard let sself = self else { return }
            guard let heroes = heroes else {
                let sortSavedHeroes = HeroEntity.pullHeroEntity(startIndex: 0, capacity: loadCapacity)
                guard sortSavedHeroes.count > 0 else {
                    sself.present(UIAlertController.createErorAlert(error: error!), animated: true, completion: nil)
                    sself.refreshControl.endRefreshing()
                    return
                }
                sself.heroesCollectionArray = sortSavedHeroes
                sself.collectionOfHeros.reloadData()
                sself.refreshControl.endRefreshing()
                return
            }
            sself.heroesCollectionArray = heroes
            sself.collectionOfHeros.reloadData()
            NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
            sself.refreshControl.endRefreshing()
        })
    }
}
