//
//  StoriesViewController.swift
//  marvel-ios
//
//  Created by Admin on 11.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import MagicalRecord

private let reuseIdentifier = "itemCellID"
private let loadCapacity = 20

class ItemsViewController: UITableViewController, IndicatorInfoProvider {
    
    // MARK: - Variables
    var itemInfo: IndicatorInfo = "View"
    var heroID : Int32 = 0
    var sectionName : String
    var items : [ItemEntity] = []
    var loadMoreStatus = false
    let requestManager = RequestManager()
    
    init(itemInfo: IndicatorInfo, heroID: Int32, sectionName: String) {
        self.heroID = heroID
        self.sectionName = sectionName
        self.itemInfo = itemInfo
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let itemCellNib = UINib(nibName: "ItemTableViewCell", bundle: nil)
        self.tableView?.register(itemCellNib, forCellReuseIdentifier: reuseIdentifier)
        
        //refreshControl
        refreshControl = UIRefreshControl()
        refreshControl?.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl?.addTarget(self, action: #selector(refreshCollectionView(sender:)), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl!) // not required when using UITableViewController
        
        //infinite scroll view
        let screenSize = UIScreen.main.bounds.size
        let frame = CGRect(x: 0, y: 0, width: screenSize.width, height: 77)
        tableView.tableFooterView = IndicatorView.create(frame: frame)
        tableView.tableFooterView?.isHidden = true
        
        requestManager.getFullHeroInformation(heroID: heroID, sectionName: sectionName, offset: items.count ?? 0) { [weak self] (items, error) in
            guard let sself = self else { return }
            guard let items = items else {
                print(error?.localizedDescription ?? "Error")
                let sortSavedItems = ItemEntity.pullItemEntity(heroID: sself.heroID, sectionName: sself.sectionName, startIndex: 0, capacity: loadCapacity)
                guard sortSavedItems.count > 0 else {
                    sself.present(UIAlertController.createErorAlert(error: error!), animated: true, completion: nil)
                    return
                }
                sself.items = sortSavedItems
                sself.tableView.reloadData()
                return
            }
            sself.items = items
            sself.tableView.reloadData()
            NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - UITableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.item]
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! ItemTableViewCell
        cell.item = item
        return cell
     }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height < tableView.frame.size.height ? tableView.frame.size.height : scrollView.contentSize.height
        let maxiHeight = contentHeight - tableView.frame.size.height
        loadMoreStatus = offsetY < 2/3 * maxiHeight ? false : loadMoreStatus
        if offsetY > 2/3 * maxiHeight && !loadMoreStatus {
            loadAndDrawNewHeroes()
        }
    }
    
    func loadAndDrawNewHeroes()
    {
        loadMoreStatus = true
        var indexPaths: [IndexPath] = []
        self.tableView.tableFooterView?.isHidden = false
        let offset = self.items.count ?? 0
        self.requestManager.getFullHeroInformation(heroID: self.heroID, sectionName: self.sectionName, offset: offset, { [weak self] (newItems, error) in
            guard let sself = self else { return }
            sself.loadMoreStatus = true
            sself.tableView.tableFooterView?.isHidden = true
            guard let newItems = newItems else {
                let sortSavedItems = ItemEntity.pullItemEntity(heroID: sself.heroID, sectionName: sself.sectionName, startIndex: offset, capacity: loadCapacity)
                guard sortSavedItems.count > 0 else {
                    print(error?.localizedDescription ?? "Error")
                    sself.present(UIAlertController.createErorAlert(error: error!), animated: true, completion: nil)
                    sself.tableView.tableFooterView?.isHidden = true
                    return
                }
                sortSavedItems.forEach({ (item) in
                    sself.items.append(item)
                    indexPaths.append(IndexPath.init(item: (sself.items.endIndex)-1, section: 0))
                })
                sself.tableView.insertRows(at: indexPaths, with: UITableViewRowAnimation.none)
                sself.tableView.tableFooterView?.isHidden = true
                return
            }
            newItems.forEach({ (item) in
                sself.items.append(item)
                indexPaths.append(IndexPath.init(item: (sself.items.endIndex)-1, section: 0))
            })
            sself.tableView.insertRows(at: indexPaths, with: .none)
            NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
            
            if(newItems.count == 0)
            {
                guard (sself.items.endIndex)>0 else {return}
                sself.tableView.tableFooterView?.isHidden = true
                sself.tableView.scrollToRow(at: IndexPath.init(item: (sself.items.endIndex)-1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
            }
        })
    }
    
    //MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    //MARK: - UIRefreshControl
    func refreshCollectionView(sender:AnyObject) {
        self.requestManager.getFullHeroInformation(heroID: self.heroID, sectionName: self.sectionName, offset: 0, { [weak self] (items, error) in
            guard let sself = self else { return }
            guard let items = items else {
                print(error?.localizedDescription ?? "Error")
                let sortSavedItems = ItemEntity.pullItemEntity(heroID: sself.heroID, sectionName: sself.sectionName, startIndex: 0, capacity: loadCapacity)
                guard sortSavedItems.count > 0 else {
                    sself.present(UIAlertController.createErorAlert(error: error!), animated: true, completion: nil)
                    sself.refreshControl?.endRefreshing()
                    return
                }
                sself.items = sortSavedItems
                sself.tableView.reloadData()
                sself.refreshControl?.endRefreshing()
                return
            }
            sself.items = items
            sself.tableView.reloadData()
            NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
            sself.refreshControl?.endRefreshing()
        })
        
    }
    
}
