//
//  RequestManager.swift
//  marvel-ios
//
//  Created by Admin on 09.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

private let generalHost = "https://gateway.marvel.com"
private let publicKey = "1ba6acbd07884a6e8cd019e068b212db"
private let privateKey = "6a244c1b3ae1a7190ae98f764368a0839589290c"

enum error: Error{
    case overflow
    case invalidInput(String)
}

extension String{
    //get MD5 from key
    func MD5(string: String) -> Data? {
        guard let messageData = string.data(using:String.Encoding.utf8) else { return nil }
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        return digestData
    }
}

class RequestManager: NSObject {
    
    func getHeroes(offset: Int, _ completion: @escaping (_ heroes: [HeroEntity]?, _ error: Error?) -> ()) {
        
        let urlAddress = URL.init(string: generalHost.appending("/v1/public/characters"))!
        let parameters = getParameters(ts: "1", offset: offset)
        var heroes = [HeroEntity]()
        
        Alamofire.request(urlAddress, parameters: parameters).responseJSON( completionHandler: {  (response: DataResponse) in
            switch(response.result){
            case .success( _):
                
                let dic = response.result.value as! NSDictionary
                let diction = (dic.value(forKey: "data") as! NSDictionary).value(forKey: "results") as! NSArray
                diction.forEach({ (hero) in
                    let map = Map.init(mappingType: MappingType.fromJSON, JSON: hero as! [String : Any])
                    guard HeroEntity.isExist(id: (hero as! NSDictionary).value(forKey: "id") as! Int) else {
                        heroes.append(HeroEntity.updateHeroEntity(map: map))
                        return
                    }
                    heroes.append(HeroEntity.init(map: map)!)
                })
                completion(heroes, nil)
                break;
            case .failure( _):
                print("FAILURE: ", response.result.error?.localizedDescription ?? "Error!")
                completion(nil, response.result.error)
                break;
            }
        })
    }
    
    func getFullHeroInformation(heroID: Int32, sectionName: String, offset: Int, _ completion: @escaping (_ items: [ItemEntity]?, _ error: Error?) -> ()) {
        
        let urlAdress = URL.init(string: String.init(format: "%@/v1/public/characters/%@/%@", generalHost, String.init(heroID), sectionName))!
        let parameters = getParameters(ts: "1", offset: offset)
        var items = [ItemEntity]()
        
        Alamofire.request(urlAdress, parameters: parameters).responseJSON( completionHandler: {  (response: DataResponse) in
            switch(response.result){
            case .success( _):
                let dic = response.result.value as! NSDictionary
                let diction = (dic.value(forKey: "data") as! NSDictionary).value(forKey: "results") as! NSArray
                diction.forEach({ (item) in
                    let map = Map.init(mappingType: MappingType.fromJSON, JSON: item as! [String : Any])
                    let itemID = (item as! NSDictionary).value(forKey: "id").unsafelyUnwrapped as! Int32
                    HeroAndItems.init(heroID: heroID, itemID: itemID)
                    guard ItemEntity.isExist(id: (item as! NSDictionary).value(forKey: "id") as! Int) else {
                        items.append(ItemEntity.updateItemEntity(map: map))
                        return
                    }
                    guard let newItem = ItemEntity.init(map: map) else {return}
                    newItem.section = sectionName
                    items.append(newItem)
                })
                completion(items, nil)
                break;
            case .failure( _):
                print("FAILURE: ", response.result.error?.localizedDescription ?? "Error")
                completion(nil, response.result.error)
                break;
            }
        })
    }
    
    func getParameters(ts: String, offset: Int) -> Parameters{
        var hash = String(format: "%@%@%@", ts, privateKey, publicKey)
        let md5Data = hash.MD5(string: hash)
        let md5Hex =  md5Data!.map { String(format: "%02hhx", $0) }.joined()
        hash = md5Hex
        print("md5 from hash: \(md5Hex)")
        
        let parameters = Parameters.init(dictionaryLiteral: ("ts",ts), ("apikey", publicKey), ("hash", md5Hex), ("offset", offset))
        return parameters
    }
}
