//
//  IndicatorView.swift
//  marvel-ios
//
//  Created by Admin on 15.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit

class IndicatorView: UIView {
    // MARK: - IBOutlets
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    static func create(frame: CGRect) -> IndicatorView {
        let view = Bundle.main.loadNibNamed("IndicatorView", owner: nil, options: nil)?.first as! IndicatorView
        view.frame = frame
        return view
    }
}
