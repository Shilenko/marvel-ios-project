//
//  ItemEntity+CoreDataClass.swift
//  marvel-ios
//
//  Created by Admin on 21.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import Foundation
import CoreData
import ObjectMapper

public class ItemEntity: NSManagedObject, Mappable {
    
    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
    
    //MARK: - NSManagedObject
    required public init?(map: Map) {
        let ctx = NSManagedObjectContext.mr_default()
        let entity = NSEntityDescription.entity(forEntityName: "ItemEntity", in: ctx)
        super.init(entity: entity!, insertInto: ctx)
        mapping(map: map)
        guard let value = map["id"].currentValue, value is Int32 else { return }
        guard (ItemEntity.mr_find(byAttribute: "id", withValue: value, in: NSManagedObjectContext.mr_default()) == nil) else {return}
        ctx.insert(self)
    }
    
    //MARK: - Mappable
    public func mapping(map: Map) {
        id              <- map["id"]
        title           <- map["title"]
        itDescription   <- map["description"]
        pageCount       <- map["pageCount"]
        imagePath       <- map["thumbnail.path"]
        imageExtension  <- map["thumbnail.extension"]
    }

    //MARK: - for ItemsViewController
    public static func pullItemEntity(heroID: Int32, sectionName: String, startIndex: Int, capacity: Int) -> [ItemEntity]
    {
        guard let indexes = HeroAndItems.mr_find(byAttribute: "heroID", withValue: heroID) as? [HeroAndItems] else {return []}
        var mass = ItemEntity.mr_find(byAttribute: "section", withValue: sectionName) as! [ItemEntity]
        mass = mass.filter { (item) -> Bool in
            item.isIndexesExist(indexes: indexes)
        }
        mass.sort { (first, second) -> Bool in
            return first.title! < second.title!
        }

        var result = [ItemEntity]()
        for (index, item) in mass.enumerated() {
            if(index >= startIndex && index < startIndex + capacity)
            {
                result.append(item)
            }
        }
        return result
    }
    
    public func isIndexesExist(indexes: [HeroAndItems]) -> Bool{
        var result = false
        indexes.forEach { (index) in
            if index.itemID == self.id {
                result = true
                return
            }
        }
        return result
    }
    
    public static func isExist(id: Int) -> Bool{
        let items = ItemEntity.mr_find(byAttribute: "id", withValue: id)
        guard items?.count != 0 else {return true}
        return false
    }
    
    public static func updateItemEntity(map: Map) -> ItemEntity{
        let id  = map["id"].currentValue.unsafelyUnwrapped
        let matchItems = ItemEntity.mr_find(byAttribute: "id", withValue: id) as! [ItemEntity]
        guard matchItems.count == 1 else {return ItemEntity()}
        let item = matchItems[0]
        item.mapping(map: map)
        return item
    }
}
