//
//  HeroEntity+CoreDataProperties.swift
//  marvel-ios
//
//  Created by Admin on 21.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import Foundation
import CoreData


extension HeroEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<HeroEntity> {
        return NSFetchRequest<HeroEntity>(entityName: "HeroEntity");
    }

    @NSManaged public var id: Int32
    @NSManaged public var imageExtension: String?
    @NSManaged public var imagePath: String?
    @NSManaged public var name: String?
    @NSManaged public var newRelationship: HeroEntity?

}
