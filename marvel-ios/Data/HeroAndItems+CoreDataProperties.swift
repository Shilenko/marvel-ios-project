//
//  HerosAndItems+CoreDataProperties.swift
//  marvel-ios
//
//  Created by Admin on 22.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import Foundation
import CoreData

extension HeroAndItems {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<HeroAndItems> {
        return NSFetchRequest<HeroAndItems>(entityName: "HeroAndItems");
    }

    @NSManaged public var heroID: Int32
    @NSManaged public var itemID: Int32
    @NSManaged public var heroRelationship: HeroEntity?
    @NSManaged public var itemRelationship: ItemEntity?

}
