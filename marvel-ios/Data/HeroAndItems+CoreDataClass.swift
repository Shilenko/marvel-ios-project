//
//  HerosAndItems+CoreDataClass.swift
//  marvel-ios
//
//  Created by Admin on 22.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import Foundation
import CoreData

public class HeroAndItems: NSManagedObject {
    
    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
    
    public init(heroID: Int32, itemID: Int32)
    {
        let ctx = NSManagedObjectContext.mr_default()
        let entity = NSEntityDescription.entity(forEntityName: "HeroAndItems", in: ctx)
        super.init(entity: entity!, insertInto: ctx)
        
        let heroPredicate = NSPredicate(format: "heroID = %d", heroID)
        let itemPredicate = NSPredicate(format: "itemID = %d", itemID)
        let compound = NSCompoundPredicate.init(andPredicateWithSubpredicates: [heroPredicate, itemPredicate])

        guard (HeroAndItems.mr_findAll(with: compound, in:NSManagedObjectContext.mr_default()) as! [HeroAndItems]).count == 0 else {return}
        
        self.heroID = heroID
        self.itemID = itemID
        
//        guard let heroRel = HeroEntity.mr_find(byAttribute: "id", withValue: heroID) as? [HeroEntity] else {return}
//        guard heroRel.count == 1 else {return}
//        self.heroRelationship = heroRel[0]
//        
//        guard let itemRel = HeroEntity.mr_find(byAttribute: "id", withValue: itemID) as? [ItemEntity] else {return}
//        guard itemRel.count == 1 else {return}
//        self.itemRelationship = itemRel[0]
        
        ctx.insert(self)
        do{
            try self.managedObjectContext?.save()
        }catch{
            let saveError = error as NSError
            print(saveError)
        }
    }
}
