//
//  HeroEntity+CoreDataClass.swift
//  marvel-ios
//
//  Created by Admin on 18.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import Foundation
import CoreData
import ObjectMapper


public class HeroEntity: NSManagedObject, Mappable {
    
    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
    
    //MARK: - NSManagedObject
    required public init?(map: Map) {
        let ctx = NSManagedObjectContext.mr_default()
        let entity = NSEntityDescription.entity(forEntityName: "HeroEntity", in: ctx)
        super.init(entity: entity!, insertInto: ctx)
        mapping(map: map)
        let searchValue = map["id"]
        guard let value = searchValue.currentValue, value is Int32 else { return }
        guard (HeroEntity.mr_find(byAttribute: "id", withValue: value, in: NSManagedObjectContext.mr_default()) == nil) else {return}
        ctx.insert(self)
    }
    
    //MARK: - Mappable
    public func mapping(map: Map) {
        id              <- map["id"]
        name            <- map["name"]
        imagePath       <- map["thumbnail.path"]
        imageExtension  <- map["thumbnail.extension"]
    }

    
    //MARK: - for HerosCollectionViewController
    public static func pullHeroEntity(startIndex: Int, capacity: Int) -> [HeroEntity]
    {
        var mass = [HeroEntity]()
        var heroes = HeroEntity.mr_findAll(in: NSManagedObjectContext.mr_default()) as! [HeroEntity]
        guard heroes.count != 0 else {return []}
        heroes = heroes.sorted { (first, second) -> Bool in
            first.name! < second.name!
        }
        for (index, hero) in heroes.enumerated() {
            if(index >= startIndex && index < startIndex + capacity)
            {
                mass.append(hero)
            }
        }
        return mass
    }
    
    public static func isExist(id: Int) -> Bool{
        let heroes = HeroEntity.mr_find(byAttribute: "id", withValue: id)
        guard heroes?.count != 0 else {return true}
        return false
    }
    
    public static func updateHeroEntity(map: Map) -> HeroEntity{
        let id  = map["id"].currentValue.unsafelyUnwrapped
        let matchHeroes = HeroEntity.mr_find(byAttribute: "id", withValue: id) as! [HeroEntity]
        guard matchHeroes.count == 1 else {return HeroEntity()}
        let hero = matchHeroes[0]
        hero.mapping(map: map)
        return hero
    }
}
