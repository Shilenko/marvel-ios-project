//
//  ItemEntity+CoreDataProperties.swift
//  marvel-ios
//
//  Created by Admin on 21.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import Foundation
import CoreData


extension ItemEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ItemEntity> {
        return NSFetchRequest<ItemEntity>(entityName: "ItemEntity");
    }

    @NSManaged public var id: Int32
    @NSManaged public var imageExtension: String?
    @NSManaged public var imagePath: String?
    @NSManaged public var itDescription: String?
    @NSManaged public var pageCount: Int32
    @NSManaged public var section: String?
    @NSManaged public var title: String?
    @NSManaged public var newRelationship: ItemEntity?

}
