//
//  HeroFullInformation.swift
//  marvel-ios
//
//  Created by Admin on 11.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import Foundation
import XLPagerTabStrip

class HeroFullInformationController: ButtonBarPagerTabStripViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var heroPic: UIImageView!
    @IBOutlet weak var heroName: UILabel!
    
    //MARK: - Variables
    var hero : HeroEntity? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonBarView.selectedBar.backgroundColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        buttonBarView.backgroundColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
        
        if(hero != nil){
            heroName.attributedText = NSAttributedString.init(string: (hero?.name) ?? "Hero name")
            
            guard let imagePath = hero?.imagePath else {return}
            guard let imageExtension = hero?.imageExtension else {return}
            guard let imageURL = URL.init(string: String(format: "%@.%@", imagePath, imageExtension)) else { return }
            print("URL for image: ",imageURL)
            heroPic.kf.setImage(with: imageURL)
            heroPic.contentMode = .scaleAspectFill
            heroPic.layer.borderColor = UIColor.lightGray.cgColor
            heroPic.layer.borderWidth = 1.0
            heroPic.layer.masksToBounds = false
            heroPic.layer.cornerRadius = heroPic.frame.size.height/2
            heroPic.clipsToBounds = true
        }
        
        settings.style.buttonBarBackgroundColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
        settings.style.buttonBarItemBackgroundColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .white
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController]{
        let comics = ItemsViewController(itemInfo: "Comics", heroID: (hero?.id)!, sectionName: "comics")
        let series = ItemsViewController(itemInfo: "Series", heroID: (hero?.id)!, sectionName: "series")
        let stories = ItemsViewController(itemInfo: "Stories", heroID: (hero?.id)!, sectionName: "stories")
        let events = ItemsViewController(itemInfo: "Events", heroID: (hero?.id)!, sectionName: "events")
        
        return [comics, series, stories, events]
    }
}
