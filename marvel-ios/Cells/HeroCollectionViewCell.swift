//
//  HeroCollectionViewCell.swift
//  marvel-ios
//
//  Created by Admin on 10.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit
import Kingfisher

class HeroCollectionViewCell: UICollectionViewCell {
    
    // MARK:- IBOutlets
    @IBOutlet weak var imageFrameView: UIView!
    @IBOutlet weak fileprivate var photoImageView: UIImageView!
    @IBOutlet weak fileprivate var nameLabel: UILabel!
    
    // MARK:- Public
    var hero: HeroEntity? {
        didSet {
            
            guard let imagePath = hero?.imagePath else {return}
            guard let imageExtension = hero?.imageExtension else {return}
            guard let imageURL = URL.init(string: String(format: "%@.%@", imagePath, imageExtension)) else { return }
            photoImageView.kf.setImage(with: imageURL)
            photoImageView.contentMode = .scaleAspectFill
            
            imageFrameView.layer.borderColor = Color.lightGray.cgColor
            imageFrameView.layer.borderWidth = 1.0
            imageFrameView.layer.masksToBounds = true
            imageFrameView.layer.cornerRadius = self.frame.size.width/2
            imageFrameView.layer.shouldRasterize = true
//            imageFrameView.clipsToBounds = true
            
            nameLabel.text = hero?.name
        }
    }
    
    //image parallax
    func offset(offset: CGPoint) {
        photoImageView.frame = self.photoImageView.bounds.offsetBy(dx: photoImageView.frame.origin.x, dy: offset.y)
        photoImageView.contentMode = UIViewContentMode.scaleAspectFill
    }
}
